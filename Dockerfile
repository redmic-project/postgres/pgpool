FROM alpine:3.7

ENV PGPOOL_VERSION="3.7.3" \
    PG_VERSION="10.4-r0" \
    PCP_USER="postgres" \
    PCP_PASSWORD="password" \
    PCP_PORT="9898" \
    PGPOOL_PORT="9999" \
    PGPOOL_SR_CHECK_PERIOD="10" \
    PGPOOL_SR_CHECK_USER="postgres" \
    PGPOOL_SR_CHECK_PASSWORD="password" \
    PGPOOL_SR_CHECK_DATABASE="postgres" \
    PGPOOL_DELAY_THRESHOLD="10000000" \
    PGPOOL_LOG_STANDBY_DELAY="if_over_threshold" \
    PGPOOL_BACKENDS="1:localhost:5432" \
    PGPOOL_PATH="/etc/pgpool2"


COPY scripts /scripts

RUN apk --update --no-cache add \
        bash \
        libpq=${PG_VERSION} \
        postgresql-dev=${PG_VERSION} \
        postgresql-client=${PG_VERSION} \
        linux-headers \
        gcc \
        make \
        libgcc \
        g++ \
        libffi-dev \
        gettext \
        \
    && cd /tmp \
    && wget http://www.pgpool.net/mediawiki/images/pgpool-II-${PGPOOL_VERSION}.tar.gz -O - | tar -xz \
    && chown root:root -R /tmp/pgpool-II-${PGPOOL_VERSION} \
    && cd /tmp/pgpool-II-${PGPOOL_VERSION} \
    && ./configure --prefix=/usr \
                   --sysconfdir=/etc \
                   --mandir=/usr/share/man \
                   --infodir=/usr/share/info \
    && make \
    && make install \
    && rm -rf /tmp/pgpool-II-${PGPOOL_VERSION} \
    && apk del postgresql-dev linux-headers gcc make libgcc g++ \
    \
    && chmod +x /scripts/docker-entrypoint.sh \
    && mkdir -p ${PGPOOL_PATH} \
    && mkdir -p /var/run/pgpool

EXPOSE ${PGPOOL_PORT}

ENTRYPOINT ["/scripts/docker-entrypoint.sh"]

CMD ["sh", "-c", "pgpool -n -f ${PGPOOL_PATH}/pgpool.conf -F ${PGPOOL_PATH}/pcp.conf -a ${PGPOOL_PATH}/pool_hba.conf"]