#!/bin/bash

set -e

FILENAME_PGPOOL="pgpool"
FILENAME_POOL_HBA="pool_hba"

envsubst < /scripts/${FILENAME_PGPOOL}.template > ${PGPOOL_PATH}/${FILENAME_PGPOOL}.conf
envsubst < /scripts/${FILENAME_POOL_HBA}.template > ${PGPOOL_PATH}/${FILENAME_POOL_HBA}.conf

pg_md5 --md5auth --username=${PCP_USERNAME} --config-file=${PGPOOL_PATH}/${FILENAME_PGPOOL}.conf ${PCP_PASSWORD}

IFS=';' read -ra CONNECTIONS <<< "${PGPOOL_BACKENDS}"

i=0
for CON in "${CONNECTIONS[@]}"; do
	IFS=':' read -ra CONF <<< "$CON"
	cat >> ${PGPOOL_PATH}/${FILENAME_PGPOOL}.conf <<- EOM

	backend_hostname$i = '${CONF[1]}'
	backend_port$i = '${CONF[2]}'
	backend_weight$i = '${CONF[0]}'
	backend_flag$i = 'ALLOW_TO_FAILOVER'
	EOM

	i=$((i+1))
done

exec "$@"